# Vim syntax highlighting plugin for Meson build system files

This is a copy of the upstream syntax highlighting files, extracted into a separate Git repository for easy use with Vim plugin managers such as [Vundle](https://github.com/VundleVim/Vundle.vim).

The upstream files are available here:
https://github.com/mesonbuild/meson/tree/master/data/syntax-highlighting/vim

## How to use with Vundle

Add this to your `.vimrc`:

```
Plugin 'git@gitlab.com:mrubli/vim-syntax-meson.git'
```